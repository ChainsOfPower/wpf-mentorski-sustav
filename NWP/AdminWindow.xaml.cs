﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {

        public AdminWindow()
        {
            InitializeComponent();
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StudentiWindow sw = new StudentiWindow();
            this.Close();
            sw.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ProfesoriWindow pw = new ProfesoriWindow();
            this.Close();
            pw.Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            KolegijiWindow kw = new KolegijiWindow();
            this.Close();
            kw.Show();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            AkademskaGodinaWindow agw = new AkademskaGodinaWindow();
            this.Close();
            agw.Show();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            this.Close();
            mw.Show();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            UpisniListWindow ul = new UpisniListWindow();
            this.Close();
            ul.Show();
        }
    }
}
