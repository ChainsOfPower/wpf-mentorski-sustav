﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Interaction logic for AkademskaGodinaWindow.xaml
    /// </summary>
    public partial class AkademskaGodinaWindow : Window
    {
        
        AkademskaGodina ag = new AkademskaGodina();

        public AkademskaGodinaWindow()
        {
            InitializeComponent();
            unos.DataContext = ag;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {

                ctx.AkademskaGodina.Add(ag);
                ag = new AkademskaGodina();
                unos.DataContext = ag;
                ctx.SaveChanges();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AdminWindow aw = new AdminWindow();
            aw.Show();
            this.Close();
        }
    }
}
