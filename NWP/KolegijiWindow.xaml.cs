﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Interaction logic for KolegijiWindow.xaml
    /// </summary>
    public partial class KolegijiWindow : Window
    {
        ObservableCollection<Kolegij> kolegiji;
        ObservableCollection<Profesor> profesori;
        ObservableCollection<Semestar> semestri;
        ObservableCollection<Godina> godine;

        public KolegijiWindow()
        {
            InitializeComponent();

            using (var ctx = new MentorskiSustavContext())
            {
                var query1 = from p in ctx.Profesori
                             select p;
                profesori = new ObservableCollection<Profesor>(query1.ToList());
                cboProfesori.ItemsSource = profesori;

                var query2 = from k in ctx.Kolegiji
                             select k;
                kolegiji = new ObservableCollection<Kolegij>(query2.ToList());
                cboKolegiji.ItemsSource = kolegiji;

                var query3 = from s in ctx.Semestar
                             select s;
                semestri = new ObservableCollection<Semestar>(query3.ToList());
                cboSemestri.ItemsSource = semestri;

                var query4 = from g in ctx.Godina
                             select g;
                godine = new ObservableCollection<Godina>(query4.ToList());
                cboGodine.ItemsSource = godine;
            }
        }

        private void PohraniPromjene(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {
                Kolegij odabrani = (Kolegij)cboKolegiji.SelectedItem;
                if (odabrani == null)
                    return;
                string id = odabrani.KolegijId;

                var ko = ctx.Kolegiji.First(k => k.KolegijId.Equals(id));
                ko.NazivPredmeta = odabrani.NazivPredmeta;
                ko.ECTS = odabrani.ECTS;
                ko.UkupnoSati = odabrani.UkupnoSati;
                ko.ProfesorId = odabrani.Profesor.ProfesorId;
                ko.SemestarId = odabrani.Semestar.SemestarId;
                ko.GodinaId = odabrani.Godina.GodinaId;

                ctx.SaveChanges();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {
                Kolegij odabrani = (Kolegij)cboKolegiji.SelectedItem;
                string id = odabrani.KolegijId;
                var ko = ctx.Kolegiji.First(k => k.KolegijId.Equals(id));

                ctx.Kolegiji.Remove(ko);
                try
                {
                    ctx.SaveChanges();
                    kolegiji.Remove((Kolegij)cboKolegiji.SelectedItem);
                }
                catch (Exception)
                {
                    MessageBox.Show("Kolegij se nalazi na upisnim listovima te ne može biti izbrisan!");
                    return;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AdminWindow aw = new AdminWindow();
            aw.Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            NoviKolegij nk = new NoviKolegij();
            nk.Show();
            this.Close();
        }
    }
}
