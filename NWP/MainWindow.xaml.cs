﻿using NWP.PasswordHashing;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<Profesor> profesori;
        string unesenaSifra;
        public MainWindow()
        {
            InitializeComponent();
            Database.SetInitializer(new DbInitializer());

            var ctx = new MentorskiSustavContext();
            //System.Console.WriteLine(ctx.Database.Connection.ConnectionString);
            var query = from p in ctx.Profesori
                        select p;
            profesori = new ObservableCollection<Profesor>(query.ToList());
            combo.ItemsSource = profesori;

        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Profesor odabraniProfesor = (Profesor)combo.SelectedItem;
            unesenaSifra = txtPassword.Password;

            if (odabraniProfesor == null)
            {
                MessageBox.Show("Odaberite korisnika!");
            }
            else
            {
                if (PasswordHasher.IsPasswordValid(unesenaSifra, odabraniProfesor.Lozinka))
                {
                    unesenaSifra = "";

                    if (odabraniProfesor.Rola.RolaId.Equals("Admin"))
                    {
                        AdminWindow aw = new AdminWindow();
                        this.Close();
                        aw.Show();
                    }
                    else
                    {
                        MentorWindow mw = new MentorWindow(odabraniProfesor);
                        this.Close();
                        mw.Show();
                    }

                }
                else
                {
                    MessageBox.Show("Unjeli ste pogrešnu lozinku!");
                }
            }
        }
    }
}
