﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Interaction logic for MentorWindow.xaml
    /// </summary>
    public partial class MentorWindow : Window
    {
        public ObservableCollection<Student> studenti;
        public ObservableCollection<AkademskaGodina> akademskeGodine;

        public ObservableCollection<UpisniList> upisniListZimski;
        public ObservableCollection<UpisniList> upisniListLjetni;

        public ObservableCollection<Kolegij> ZimskiKolegiji;
        public ObservableCollection<Kolegij> LjetniKolegiji;

        public Student odabraniStudent { get; set; }
        public AkademskaGodina odabranaAkademskaGodina { get; set; }

        public UpisniList zimskiUpisniList { get; set; }
        public UpisniList ljetniUpisniList { get; set; }

        public MentorWindow(Profesor mentor)
        {
            InitializeComponent();

            using (var ctx = new MentorskiSustavContext())
            {
                var query = from s in ctx.Studenti where s.ProfesorId.Equals(mentor.ProfesorId)
                            select s;
                studenti = new ObservableCollection<Student>(query.ToList());
                cboStudenti.ItemsSource = studenti;

                var query2 = from g in ctx.AkademskaGodina
                             select g;
                akademskeGodine = new ObservableCollection<AkademskaGodina>(query2.ToList());
                cboAkademskeGodine.ItemsSource = akademskeGodine;
                cboZimskaAkademskaGodina.ItemsSource = akademskeGodine;
                cboLjetnaAkademskaGodina.ItemsSource = akademskeGodine;

                var query3 = from k in ctx.Kolegiji
                             where k.SemestarId.Equals("Zimski")
                             select k;
                ZimskiKolegiji = new ObservableCollection<Kolegij>(query3.ToList());
                cboNoviZimskiKolegij.ItemsSource = ZimskiKolegiji;

                var query4 = from k in ctx.Kolegiji
                             where k.SemestarId.Equals("Ljetni")
                             select k;
                LjetniKolegiji = new ObservableCollection<Kolegij>(query4.ToList());
                cboNoviLjetniKolegij.ItemsSource = LjetniKolegiji;
            }


            zimskiUpisniList = new UpisniList() { Datum = DateTime.Now, Napomena = "" };
            ljetniUpisniList = new UpisniList() { Datum = DateTime.Now, Napomena = "" };
            odabraniStudent = new Student();
            odabranaAkademskaGodina = new AkademskaGodina();

            gdZimski.DataContext = zimskiUpisniList;
            gdLjetni.DataContext = ljetniUpisniList;
            cboStudenti.DataContext = odabraniStudent;
            cboAkademskeGodine.DataContext = odabranaAkademskaGodina;

        }


        private void osvjeziUL(object sender, EventArgs e)
        {
            if (cboAkademskeGodine.SelectedItem == null || cboStudenti.SelectedItem == null)
                return;
            using (var ctx = new MentorskiSustavContext())
            {
                var query = from ul in ctx.UpisniList
                            where ul.Kolegij.SemestarId.Equals("Zimski") && ul.StudentId == odabraniStudent.StudentId && ul.AkademskaGodinaId == odabranaAkademskaGodina.AkademskaGodinaId
                            select ul;
                upisniListZimski = new ObservableCollection<UpisniList>(query.ToList());
                dg1.ItemsSource = upisniListZimski;

                var query2 = from ul in ctx.UpisniList
                             where ul.Kolegij.SemestarId.Equals("Ljetni") && ul.StudentId == odabraniStudent.StudentId && ul.AkademskaGodinaId == odabranaAkademskaGodina.AkademskaGodinaId
                             select ul;
                upisniListLjetni = new ObservableCollection<UpisniList>(query2.ToList());
                dg2.ItemsSource = upisniListLjetni;


            }
        }



       

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            using (var ctx = new MentorskiSustavContext())
            {


                zimskiUpisniList.StudentId = odabraniStudent.StudentId;
                zimskiUpisniList.AkademskaGodinaId = odabranaAkademskaGodina.AkademskaGodinaId;

                try
                {
                    ctx.UpisniList.Add(zimskiUpisniList);
                    ctx.SaveChanges();
                    upisniListZimski.Add(zimskiUpisniList);
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Odabrani predmet je već upisan!");
                }
                finally
                {
                    zimskiUpisniList = new UpisniList() { Datum = DateTime.Now, Napomena = "" };
                    gdZimski.DataContext = zimskiUpisniList;

                }



            }
        }



        private void PohraniPromjene(object sender, DataGridRowEditEndingEventArgs e)
        {
            UpisniList odabrani = (UpisniList)e.Row.Item;

            if (odabrani == null)
                return;


            using (var ctx = new MentorskiSustavContext())
            {

                var ko = ctx.UpisniList.First(k => k.KolegijId.Equals(odabrani.KolegijId) && k.StudentId.Equals(odabraniStudent.StudentId) && k.AkademskaGodinaId.Equals(odabrani.AkademskaGodinaId));
                ko.Datum = odabrani.Datum;
                ko.Napomena = odabrani.Napomena;
                ko.Ocjena = odabrani.Ocjena;


                ctx.SaveChanges();

            }

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {
                UpisniList odabrani = (UpisniList)dg1.SelectedItem;

                var ko = ctx.UpisniList.First(u => u.KolegijId.Equals(odabrani.KolegijId) && u.StudentId.Equals(odabraniStudent.StudentId) && u.AkademskaGodinaId.Equals(odabranaAkademskaGodina.AkademskaGodinaId));

                ctx.UpisniList.Remove(ko);
                upisniListZimski.Remove((UpisniList)dg1.SelectedItem);
                ctx.SaveChanges();
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {


                ljetniUpisniList.StudentId = odabraniStudent.StudentId;
                ljetniUpisniList.AkademskaGodinaId = odabranaAkademskaGodina.AkademskaGodinaId;

                try
                {
                    ctx.UpisniList.Add(ljetniUpisniList);
                    ctx.SaveChanges();
                    upisniListLjetni.Add(ljetniUpisniList);
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Odabrani predmet je već upisan!");
                }
                finally
                {
                    ljetniUpisniList = new UpisniList() { Datum = DateTime.Now, Napomena = "" };
                    gdLjetni.DataContext = ljetniUpisniList;

                }



            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {
                UpisniList odabrani = (UpisniList)dg2.SelectedItem;

                var ko = ctx.UpisniList.First(u => u.KolegijId.Equals(odabrani.KolegijId) && u.StudentId.Equals(odabraniStudent.StudentId) && u.AkademskaGodinaId.Equals(odabranaAkademskaGodina.AkademskaGodinaId));

                ctx.UpisniList.Remove(ko);
                upisniListLjetni.Remove((UpisniList)dg2.SelectedItem);
                ctx.SaveChanges();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mw = new MainWindow();
            this.Close();
            mw.Show();
        }
    }
}
