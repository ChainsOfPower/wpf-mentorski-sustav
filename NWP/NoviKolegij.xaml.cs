﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Interaction logic for NoviKolegij.xaml
    /// </summary>
    public partial class NoviKolegij : Window
    {
        Kolegij noviKolegij = new Kolegij();
        ObservableCollection<Profesor> profesori;
        ObservableCollection<Godina> godine;
        ObservableCollection<Semestar> semestri;

        public NoviKolegij()
        {
            InitializeComponent();

            using (var ctx = new MentorskiSustavContext())
            {
                var query1 = from p in ctx.Profesori
                            select p;
                profesori = new ObservableCollection<Profesor>(query1.ToList());
                cboProfesor.ItemsSource = profesori;

                var query2 = from g in ctx.Godina
                             select g;
                godine = new ObservableCollection<Godina>(query2.ToList());
                cboGodina.ItemsSource = godine;

                var query3 = from s in ctx.Semestar
                             select s;
                semestri = new ObservableCollection<Semestar>(query3.ToList());
                cboSemestar.ItemsSource = semestri;

                unos.DataContext = noviKolegij;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {
                

                bool has = ctx.Kolegiji.Any(k => k.KolegijId.Equals(noviKolegij.KolegijId));

                if (has)
                {
                    MessageBox.Show("Kolegij sa unesenom šifrom već postoji!");
                    return;
                }
            }

                if (noviKolegij.KolegijId == null || noviKolegij.ECTS == 0 || noviKolegij.GodinaId == null || noviKolegij.NazivPredmeta == null
                    || noviKolegij.ProfesorId == 0 || noviKolegij.SemestarId == null || noviKolegij.UkupnoSati == 0)
                {
                    MessageBox.Show("Niste ispravno popunili sva polja!");
                    return;
                }

            using (var ctx = new MentorskiSustavContext())
            {
                
                ctx.Kolegiji.Add(noviKolegij);
                noviKolegij = new Kolegij();
                unos.DataContext = noviKolegij;
                ctx.SaveChanges();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            KolegijiWindow kw = new KolegijiWindow();
            kw.Show();
            this.Close();
        }
    }
}
