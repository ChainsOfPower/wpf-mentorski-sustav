﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Interaction logic for NoviProfesor.xaml
    /// </summary>
    public partial class NoviProfesor : Window
    {
        Profesor noviProfesor = new Profesor();
        ObservableCollection<Rola> role;
        

        public NoviProfesor()
        {
            InitializeComponent();

            using (var ctx = new MentorskiSustavContext())
            {
                var query = from r in ctx.Role
                            select r;
                role = new ObservableCollection<Rola>(query.ToList());
                cboRole.ItemsSource = role;


                
              

                unos.DataContext = noviProfesor;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {


                bool has = ctx.Profesori.Any(k => k.ProfesorId.Equals(noviProfesor.ProfesorId));

                if (has)
                {
                    MessageBox.Show("Profesor sa unesenim Id već postoji!");
                    return;
                }
            }

            if (noviProfesor.ProfesorId == 0 || noviProfesor.Ime == null || noviProfesor.Lozinka == null || noviProfesor.OIB == null
                || noviProfesor.Prezime == null || noviProfesor.RolaId == null )
            {
                MessageBox.Show("Niste ispravno popunili sva polja!");
                return;
            }


            using (var ctx = new MentorskiSustavContext())
            {
                noviProfesor.Lozinka = PasswordHashing.PasswordHasher.CreatePasswordSalt(noviProfesor.Lozinka);
                ctx.Profesori.Add(noviProfesor);
                noviProfesor = new Profesor();
                unos.DataContext = noviProfesor;
                ctx.SaveChanges();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ProfesoriWindow pw = new ProfesoriWindow();
            pw.Show();
            this.Close();
        }
    }
}
