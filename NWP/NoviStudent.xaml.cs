﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Interaction logic for NoviStudent.xaml
    /// </summary>
    public partial class NoviStudent : Window
    {
        Student noviStudent = new Student();
        ObservableCollection<Profesor> mentori;
        ObservableCollection<Godina> godine;

        public NoviStudent()
        {
            InitializeComponent();


            using (var ctx = new MentorskiSustavContext())
            {
                var query = from p in ctx.Profesori
                            select p;
                mentori = new ObservableCollection<Profesor>(query.ToList());
                cboMentori.ItemsSource = mentori;


                var query2 = from g in ctx.Godina
                             select g;
                godine = new ObservableCollection<Godina>(query2.ToList());
                cboGodine.ItemsSource = godine;

                unos.DataContext = noviStudent;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {


                bool has = ctx.Studenti.Any(k => k.StudentId.Equals(noviStudent.StudentId));

                if (has)
                {
                    MessageBox.Show("Student sa unesenim brojem indexa već postoji!");
                    return;
                }
            }

            if (noviStudent.StudentId == 0 || noviStudent.GodinaId == null || noviStudent.Ime == null || noviStudent.JMBG == null
                || noviStudent.OIB == null || noviStudent.Prezime == null || noviStudent.ProfesorId == 0)
            {
                MessageBox.Show("Niste ispravno popunili sva polja!");
                return;
            }


           

            using (var ctx = new MentorskiSustavContext())
            {
                ctx.Studenti.Add(noviStudent);
                noviStudent = new Student();
                unos.DataContext = noviStudent;
                ctx.SaveChanges();
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            StudentiWindow sw = new StudentiWindow();
            sw.Show();
            this.Close();
        }
    }
}
