﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Interaction logic for ProfesoriWindow.xaml
    /// </summary>
    public partial class ProfesoriWindow : Window
    {
        ObservableCollection<Profesor> profesori;
        ObservableCollection<Rola> role;

        public ProfesoriWindow()
        {
            InitializeComponent();

            using (var ctx = new MentorskiSustavContext())
            {
               

                var query1 = from p in ctx.Profesori
                             select p;
                profesori = new ObservableCollection<Profesor>(query1.ToList());
                cboProfesori.ItemsSource = profesori;

                var query2 = from r in ctx.Role
                             select r;
                role = new ObservableCollection<Rola>(query2.ToList());
                cboRole.ItemsSource = role;
            }
        }

        private void PohraniPromjene(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {
                Profesor odabrani = (Profesor)cboProfesori.SelectedItem;
                if (odabrani == null)
                    return;
                int id = odabrani.ProfesorId;
               
                var pr = ctx.Profesori.First(p => p.ProfesorId == id);
                pr.Ime = odabrani.Ime;
                pr.Prezime = odabrani.Prezime;
                pr.OIB = odabrani.OIB;
                if(!pr.Lozinka.Equals(odabrani.Lozinka))
                {
                    string lozinka = PasswordHashing.PasswordHasher.CreatePasswordSalt(odabrani.Lozinka);
                    pr.Lozinka = lozinka;
                }
                pr.RolaId = odabrani.Rola.RolaId;

                ctx.SaveChanges();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {
                Profesor odabrani = (Profesor)cboProfesori.SelectedItem;
                int id = odabrani.ProfesorId;
                var pr = ctx.Profesori.First(p => p.ProfesorId == id);

                ctx.Profesori.Remove(pr);
                try
                {
                    ctx.SaveChanges();
                    profesori.Remove((Profesor)cboProfesori.SelectedItem);
                }
                catch (Exception)
                {
                    MessageBox.Show("Profesor održava kolegije ili je mentor nekom studentu te ne može biti izbrisan!");
                    return;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AdminWindow aw = new AdminWindow();
            aw.Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            NoviProfesor np = new NoviProfesor();
            np.Show();
            this.Close();
        }
    }
}
