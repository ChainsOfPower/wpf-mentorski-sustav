﻿/************************************************************************
 * Copyright: Hans Wolff
 *
 * License:  This software abides by the LGPL license terms. For further
 *           licensing information please see the top level LICENSE.txt 
 *           file found in the root directory of CodeReason Reports.
 *
 * Author:   Hans Wolff
 *
 ************************************************************************/

using System;
using System.Data;
using System.IO;
using System.Windows;
using System.Windows.Xps.Packaging;
using CodeReason.Reports;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Application's main form
    /// </summary>
    public partial class ReportWindow : Window
    {
        private bool _firstActivated = true;

        public Student odabraniStudent;
        public List<UpisniList> upisniListovi;
        public int ukupanECTS;
        public int ukupniBodovi;

        /// <summary>
        /// Constructor
        /// </summary>
        public ReportWindow(Student os)
        {
            InitializeComponent();
            odabraniStudent = os;
            ukupanECTS = 0;
            ukupniBodovi = 0;
        }

        /// <summary>
        /// Window has been activated
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event details</param>
        private void Window_Activated(object sender, EventArgs e)
        {
            if (!_firstActivated ) return;

            _firstActivated = false;
            

            try
            {
                ReportDocument reportDocument = new ReportDocument();

                StreamReader reader = new StreamReader(new FileStream(@"Templates\SimpleReport.xaml", FileMode.Open, FileAccess.Read));
                reportDocument.XamlData += reader.ReadToEnd();
                //reportDocument.XamlImagePath = Path.Combine(Environment.CurrentDirectory, @"Templates\");
                reader.Close();

                ReportData data = new ReportData();

                // set constant document values
                data.ReportDocumentValues.Add("PrintDate", DateTime.Now); // print date is now

                

                // sample table "Ean"
                DataTable table = new DataTable("Ean");
                table.Columns.Add("Student", typeof(string));
                table.Columns.Add("Kolegij", typeof(string));
                table.Columns.Add("ECTS", typeof(string));
                table.Columns.Add("Ocjena", typeof(string));
                table.Columns.Add("Prosjek", typeof(string));

                table.Rows.Add(new object[] { odabraniStudent.StudentId + "   " + odabraniStudent.Ime + "   " + odabraniStudent.Prezime, "/", "/", "/", "/" });
                table.Rows.Add(new object[] { "OIB: " + odabraniStudent.OIB, "/", "/", "/", "/" });

                using (var ctx = new MentorskiSustavContext())
                {
                    var query1 = from u in ctx.UpisniList
                                 where u.StudentId.Equals(odabraniStudent.StudentId)
                                 select u;

                    upisniListovi = new List<UpisniList>(query1.ToList());

                    foreach (UpisniList l in upisniListovi)
                    {
                        // randomly create some articles
                        if (l.Ocjena != 0)
                        {
                            ukupanECTS += l.Kolegij.ECTS;
                            ukupniBodovi += l.Kolegij.ECTS * l.Ocjena;
                            table.Rows.Add(new object[] { "/", l.KolegijId + "  " + l.Kolegij.NazivPredmeta, l.Kolegij.ECTS, l.Ocjena, "/" });
                        }

                    }
                }

                double p = (double)ukupniBodovi / ukupanECTS;
                double formatirani = Math.Truncate(p * 100) / 100;
                table.Rows.Add(new object[] { "/", "/", "/", "/", string.Format("{0:N2}", formatirani) });

                
                data.DataTables.Add(table);

                DateTime dateTimeStart = DateTime.Now; // start time measure here

                XpsDocument xps = reportDocument.CreateXpsDocument(data);
                documentViewer.Document = xps.GetFixedDocumentSequence();

                // show the elapsed time in window title
                Title += " - generated in " + (DateTime.Now - dateTimeStart).TotalMilliseconds + "ms";
            }
            catch (Exception ex)
            {
                // show exception
                MessageBox.Show(ex.Message + "\r\n\r\n" + ex.GetType() + "\r\n" + ex.StackTrace, ex.GetType().ToString(), MessageBoxButton.OK, MessageBoxImage.Stop);
            }
        }
    }
}
