﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NWP
{
    /// <summary>
    /// Interaction logic for StudentiWindow.xaml
    /// </summary>
    public partial class StudentiWindow : Window
    {
        
        ObservableCollection<Student> studenti;
        ObservableCollection<Profesor> mentori;
        ObservableCollection<Godina> godine;


        public StudentiWindow()
        {
            InitializeComponent();

            using (var ctx = new MentorskiSustavContext())
            {
                var query = from s in ctx.Studenti
                            select s;
                studenti = new ObservableCollection<Student>(query.ToList());
                cboStudenti.ItemsSource = studenti;

                var query2 = from p in ctx.Profesori
                             select p;
                mentori = new ObservableCollection<Profesor>(query2.ToList());
                cboMentori.ItemsSource = mentori;

                var query3 = from g in ctx.Godina
                             select g;
                godine = new ObservableCollection<Godina>(query3.ToList());
                cboGodine.ItemsSource = godine;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            AdminWindow aw = new AdminWindow();
            aw.Show();
            this.Close();
        }

        //ova metoda se poziva prilikom pohrane promjene, bilo klikom na dugme ili promjenom odabira studenta
        private void PohraniPromjene(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {
                Student odabrani = (Student)cboStudenti.SelectedItem;
                if (odabrani == null)
                    return;
                int id = odabrani.StudentId;
                //ovdje sam pokušao spremiti promjene bez eksplicitnog pridjeljivanja svake vrijednosti zasebno,
                //korištenjem samo promijenjenih vrijednosti unutar ObservableCollection, ali bezuspješno :(
                var st = ctx.Studenti.First(s => s.StudentId == id);
                st.Ime = odabrani.Ime;
                st.Prezime = odabrani.Prezime;
                st.OIB = odabrani.OIB;
                st.JMBG = odabrani.JMBG;
                st.ProfesorId = odabrani.Profesor.ProfesorId;
                st.GodinaId = odabrani.Godina.GodinaId;

                ctx.SaveChanges();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            using (var ctx = new MentorskiSustavContext())
            {
                Student odabrani = (Student)cboStudenti.SelectedItem;
                int id = odabrani.StudentId;
                var st = ctx.Studenti.First(s => s.StudentId == id);

                ctx.Studenti.Remove(st);
                
                try
                {
                    ctx.SaveChanges();
                    studenti.Remove((Student)cboStudenti.SelectedItem);
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Student ima upisne listove te ne može biti izbrisan!");
                    return;
                }
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            NoviStudent ns = new NoviStudent();
            ns.Show();
            this.Close();
        }
    }
}
