﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWP
{
    public class AkademskaGodina
    {
        public AkademskaGodina()
        {
            UpisniListovi = new HashSet<UpisniList>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string AkademskaGodinaId { get; set; }

        //public string AGod { get; set; }

        public virtual ICollection<UpisniList> UpisniListovi { get; set; }
    }
}
