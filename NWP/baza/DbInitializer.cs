﻿using NWP.PasswordHashing;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWP
{
    public class DbInitializer : CreateDatabaseIfNotExists<MentorskiSustavContext>
    {


       

        protected override void Seed(MentorskiSustavContext context)
        {

            string lozinka = PasswordHasher.CreatePasswordSalt("ADMIN");

            Rola r = new Rola() { RolaId = "Mentor" };
            context.Role.Add(r);

            Rola r2 = new Rola() { RolaId = "Admin" };
            context.Role.Add(r2);

            Profesor prof = new Profesor() { Ime = "Admin", Lozinka = lozinka, OIB = "11111111111", Prezime = "Admin", Rola = r2, ProfesorId = 10000 };

            context.Profesori.Add(prof);

            lozinka = PasswordHasher.CreatePasswordSalt("iruzic");
            prof = new Profesor() { Ime = "Ivica", Prezime = "Ružić", OIB = "2222222", ProfesorId = 50000, Rola = r, Lozinka = lozinka };
            context.Profesori.Add(prof);

            Godina godina = new Godina() { GodinaId = "prva" };
            context.Godina.Add(godina);

            godina = new Godina() { GodinaId = "druga" };
            context.Godina.Add(godina);

            godina = new Godina() { GodinaId = "treca" };
            context.Godina.Add(godina);

            AkademskaGodina akademskaGodina = new AkademskaGodina() { AkademskaGodinaId = "2015./2016." };
            context.AkademskaGodina.Add(akademskaGodina);

            Semestar semestar = new Semestar() { SemestarId = "Ljetni" };
            context.Semestar.Add(semestar);

            semestar = new Semestar() { SemestarId = "Zimski" };
            context.Semestar.Add(semestar);

            Student student = new Student() { StudentId = 36277, Ime = "Ivan", Prezime = "Vukman", OIB = "39457478056", JMBG = "130793565898", Godina = godina, Profesor = prof };
            context.Studenti.Add(student);

            student = new Student() { StudentId = 36500, Ime = "Marko", Prezime = "Markić", OIB = "1234506029120", JMBG = "55522255566677", Godina = godina, Profesor = prof };
            context.Studenti.Add(student);


            base.Seed(context);

        }
    }
}
