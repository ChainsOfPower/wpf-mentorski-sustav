﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWP
{
    public class Godina
    {
        public Godina()
        {
            Kolegiji = new HashSet<Kolegij>();
            Studenti = new HashSet<Student>();
        }

        //  [Key]
        //  public int GodinaId { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string GodinaId { get; set; }

        public virtual ICollection<Kolegij> Kolegiji { get; set; }
        public virtual ICollection<Student> Studenti { get; set; }
    }
}
