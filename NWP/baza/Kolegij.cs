﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWP
{
    public class Kolegij
    {
        public Kolegij()
        {
            UpisniListovi = new HashSet<UpisniList>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string KolegijId { get; set; }

        //public string SifraKolegija { get; set; }
        
        public int ProfesorId { get; set; }
        
        public string SemestarId { get; set; }
        
        public string GodinaId { get; set; }

       
        public string NazivPredmeta { get; set; }
        
        public int ECTS { get; set; }
        public int UkupnoSati { get; set; }

        public virtual ICollection<UpisniList> UpisniListovi { get; set; }


        public virtual Semestar Semestar { get; set; }


        public virtual Profesor Profesor { get; set; }

        public virtual Godina Godina { get; set; }
    }
}
