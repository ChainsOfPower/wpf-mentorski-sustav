﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWP
{
    public class MentorskiSustavContext : DbContext
    {
        public MentorskiSustavContext()
            : base()
        {
           
        }

        public DbSet<Student> Studenti { get; set; }
        public DbSet<Kolegij> Kolegiji { get; set; }
        public DbSet<Profesor> Profesori { get; set; }
        public DbSet<UpisniList> UpisniList { get; set; }
        public DbSet<AkademskaGodina> AkademskaGodina { get; set; }
        public DbSet<Godina> Godina { get; set; }
        public DbSet<Semestar> Semestar { get; set; }
        public DbSet<Rola> Role { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
        




    }
}
