﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWP
{
    public class Profesor
    {
        public Profesor()
        {
            Studenti = new HashSet<Student>();
            Kolegiji = new HashSet<Kolegij>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProfesorId { get; set; }
        
        public string RolaId { get; set; }

       
        public string Ime { get; set; }

        
        public string Prezime { get; set; }

        
        public string OIB { get; set; }

        
        public string Lozinka { get; set; }

        public virtual Rola Rola { get; set; } //određuje pravo pristupa kreiranom profesoru (admin ili mentor)

        public virtual ICollection<Student> Studenti { get; set; }
        public virtual ICollection<Kolegij> Kolegiji { get; set; }
    }
}
