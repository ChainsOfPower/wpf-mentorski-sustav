﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWP
{
    public class Rola
    {
        public Rola()
        {
            Profesori = new HashSet<Profesor>();
        }


        // [Key]
        // public int RolaId { get; set; }
        [Key]
        public string RolaId { get; set; }

        public virtual ICollection<Profesor> Profesori { get; set; }
    }
}
