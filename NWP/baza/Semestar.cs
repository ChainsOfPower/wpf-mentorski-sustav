﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWP
{
    public class Semestar
    {
        public Semestar()
        {
            Kolegiji = new HashSet<Kolegij>();
        }

        //[Key]
        //public int SemestarId { get; set; }
        [Key]
        public string SemestarId { get; set; }

        public virtual ICollection<Kolegij> Kolegiji { get; set; }

    }
}
