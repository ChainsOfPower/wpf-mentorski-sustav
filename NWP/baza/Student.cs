﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWP
{
    public class Student
    {
        public Student()
        {
            UpisniListovi = new HashSet<UpisniList>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentId { get; set; }

        //public int BrojIndeksa { get; set; }
       
        public string GodinaId { get; set; }
        
        public int ProfesorId { get; set; }
        
        public string Ime { get; set; }

       
        public string Prezime { get; set; }

      
        public string OIB { get; set; }


        public string JMBG { get; set; }




        public virtual Profesor Profesor { get; set; } //studentov mentor za upise
        public virtual Godina Godina { get; set; } //trenutna godina studija
        public virtual ICollection<UpisniList> UpisniListovi { get; set; } //student svaku godinu ispunjava novi upisni list

    }
}
