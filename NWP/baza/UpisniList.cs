﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWP
{
    public class UpisniList
    {
        public UpisniList()
        {

        }
        //primarni ključ upisnog lista je kompozicija studenta, upisanog kolegija te akademske godine. To osigurava da student svaki kolegij može upisati samo jednom tokom akademske godine

        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentId { get; set; }
        [Key, Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string KolegijId { get; set; }
        [Key, Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string AkademskaGodinaId { get; set; }

        public int Ocjena { get; set; }
        public DateTime Datum { get; set; }
        public string Napomena { get; set; }


        // [Key, Column(Order =0)]
        public virtual Student Student { get; set; }

        // [Key, Column(Order =1)]
        public virtual Kolegij Kolegij { get; set; }

        // [Key, Column(Order =2)]
        public virtual AkademskaGodina AkademskaGodina { get; set; }
    }
}
